package middleware

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

func TestLoggerMiddleware(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.New()
	actionPath := "/test"
	r.GET(actionPath, Logger("/testx"), func(c *gin.Context) {
		// assertion needed
	})

	r.GET("/skipLogging", Logger(), func(c *gin.Context) {
		c.Set("skipLogging", true)
	})

	r.GET("/error", Logger(), func(c *gin.Context) {
		c.Error(errors.New("mock error"))
	})

	r.GET("/399", Logger(), func(c *gin.Context) {
		c.Status(400)
	})

	r.GET("/401", Logger(), func(c *gin.Context) {
		c.Status(401)
	})

	r.GET("/403", Logger(), func(c *gin.Context) {
		c.Status(403)
	})

	r.GET("/499", Logger(), func(c *gin.Context) {
		c.Status(500)
	})

	req, err := http.NewRequest(http.MethodGet, actionPath+"?q=abc", nil)
	if err != nil {
		t.Errorf("Could not make GET request to %s: %s", actionPath, err)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("Expected 200 OK but got %d", w.Code)
	}

	req, _ = http.NewRequest(http.MethodGet, "/skipLogging", nil)
	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)

	req, _ = http.NewRequest(http.MethodGet, "/error", nil)
	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)

	req, _ = http.NewRequest(http.MethodGet, "/399", nil)
	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)

	req, _ = http.NewRequest(http.MethodGet, "/401", nil)
	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)

	req, _ = http.NewRequest(http.MethodGet, "/499", nil)
	w = httptest.NewRecorder()
	r.ServeHTTP(w, req)
}
