package middleware

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

var timeFormat = "02/Jan/2006:15:04:05 -0700"

// Logger a middleware to capture log within gin context
func Logger(notlogged ...string) gin.HandlerFunc {
	var skip map[string]struct{}

	if length := len(notlogged); length > 0 {
		skip = make(map[string]struct{}, length)

		for _, path := range notlogged {
			skip[path] = struct{}{}
		}
	}

	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery

		if raw != "" {
			path = fmt.Sprintf("%s?%s", path, raw)
		}

		c.Next()

		if _, ok := skip[path]; ok || c.GetBool("skipLogging") {
			return
		}

		end := time.Now()
		latency := fmt.Sprintf("%13v", end.Sub(start))
		clientIP := c.ClientIP()
		method := c.Request.Method
		userAgent := c.Request.UserAgent()
		referer := c.Request.Referer()
		statusCode := c.Writer.Status()
		comment := c.Errors.ByType(gin.ErrorTypePrivate).String()
		length := c.Writer.Size()
		if length < 0 {
			length = 0
		}

		message, _ := c.Get("RequestBodyLog")
		if message == nil {
			message = ""
		}

		entry := log.WithFields(log.Fields{
			"timestamp":  time.Now().UTC(),
			"type":       "GIN",
			"statusCode": statusCode,
			"latency":    latency,
			"clientIP":   clientIP,
			"method":     method,
			"path":       path,
			"referer":    referer,
			"userAgent":  userAgent,
			"length":     length,
		})

		if len(c.Errors) > 0 {
			entry.Error(comment)
			return
		}

		switch {
		case statusCode > 499:
			entry.Error(message)
		case statusCode == 401 || statusCode == 403:
			entry.Error(message)
		case statusCode > 399:
			entry.Warn(message)
		default:
			entry.Info(message)
		}
	}
}
