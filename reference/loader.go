package reference

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/spf13/afero"
)

// Loader wraps reference loader methods
type Loader interface {
	Open(address string) ([]byte, error)
}

// File implemented interface: Loader
// Load file reference
type File struct{}

var osfs = afero.NewOsFs()

// Open opens the file reference
func (f *File) Open(address string) ([]byte, error) {
	filename := strings.Replace(address, "file://", "", -1)

	b, err := afero.NewHttpFs(osfs).Open(filename)
	if err != nil {
		return nil, err
	}

	return afero.ReadAll(b)
}

// HTTP implemented interface: Loader
// Load http reference
type HTTP struct{}

// Open opens the http reference
func (h *HTTP) Open(address string) ([]byte, error) {
	res, err := http.Get(address)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Bad http status code: %d", res.StatusCode)
	}

	return afero.ReadAll(res.Body)
}
