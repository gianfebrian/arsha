package reference

import (
	"testing"
)

func TestURI_Parse(t *testing.T) {
	tests := []struct {
		in          string
		expectError bool
	}{
		{"file:///go/to/path", false},
		{"file:///go/to/path", false},
		{"https://google.com/search", false},
		{"https://google.com/search", false},
		{"__://error", true},
	}

	for _, tt := range tests {
		u := new(URI)
		err := u.Parse(tt.in)

		if tt.expectError {
			if err == nil {
				t.Fatal("expected some error")
			}

			continue
		}

		if err != nil {
			t.Fatal(err)
		}
	}
}

func TestURI_IsCanonical(t *testing.T) {
	tests := []struct {
		in     string
		expect bool
	}{
		{"file:///go/to/path", true},
		{"file:///go/to/path", true},
		{"https://google.com/search", true},
		{"https://google.com/search", true},
		{"/var/log", false},
		{"index.html", false},
	}

	for _, tt := range tests {
		u := new(URI)
		err := u.Parse(tt.in)
		if err != nil {
			t.Error(err)

			continue
		}

		if u.IsCanonical() != tt.expect {
			t.Errorf("expected %v to be %v, got %v", tt.in, tt.expect, u.IsCanonical())
		}
	}
}

func TestURI_GetScheme(t *testing.T) {
	tests := []struct {
		in     string
		expect string
	}{
		{"file:///go/to/path", "file"},
		{"http://google.com/search", "http"},
		{"/var/log", ""},
	}

	for _, tt := range tests {
		u := new(URI)
		err := u.Parse(tt.in)
		if err != nil {
			t.Error(err)

			continue
		}

		if u.GetScheme() != tt.expect {
			t.Errorf("expected %v to be %v, got %v", tt.in, tt.expect, u.GetScheme())
		}
	}
}
