package reference

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/spf13/afero"
)

func TestFile_Open(t *testing.T) {
	tests := []struct {
		dirname     string
		dirMode     os.FileMode
		filename    string
		fileMode    os.FileMode
		data        []byte
		expectError bool
	}{
		{"/src/a", 0755, "b", 0644, []byte("abc"), false},
		{"/src/a", 0755, "c", 0644, []byte("abc"), true},
	}

	for _, tt := range tests {
		osfs = afero.NewMemMapFs()

		osfs.MkdirAll(tt.dirname, tt.dirMode)

		filepath := fmt.Sprintf("%s/%s", tt.dirname, tt.filename)
		afero.WriteFile(osfs, filepath, tt.data, tt.fileMode)

		if tt.expectError {
			osfs.Remove(filepath)
		}

		file := new(File)
		b, err := file.Open("file://" + filepath)

		if tt.expectError {
			if err == nil {
				t.Error("expected some error")
			}

			continue
		}

		if err != nil {
			t.Error(err)

			continue
		}

		if !bytes.Equal(tt.data, b) {
			t.Errorf("expected open to get %s, got %s", tt.data, b)
		}
	}
}

func TestHTTP_Open(t *testing.T) {
	tests := []struct {
		name        string
		includeHost bool
		bindPath    string
		testPath    string
		statusCode  int
		data        []byte
		expectError bool
	}{
		{"Normal", true, "/a", "/a", http.StatusOK, []byte("abc"), false},
		{"Not found", true, "/b", "/b", http.StatusNotFound, []byte("abc"), true},
		{"No host", false, "/c", "/c", http.StatusOK, []byte("abc"), true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := new(http.ServeMux)
			server.HandleFunc(tt.bindPath, func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(tt.statusCode)
				fmt.Fprint(w, string(tt.data))

			})

			ts := httptest.NewServer(server)
			defer ts.Close()

			h := new(HTTP)
			if tt.includeHost {
				tt.testPath = fmt.Sprintf("%s/%s", ts.URL, tt.testPath)
			}

			b, err := h.Open(tt.testPath)
			if tt.expectError {
				if err == nil {
					t.Error("expect some error")
				}

				return
			}

			if err != nil {
				t.Error(err)

				return
			}

			if !bytes.Equal(tt.data, b) {
				t.Errorf("expected open to get %s, got %s", tt.data, b)
			}
		})
	}
}
