package reference

import (
	"net/url"
	"path/filepath"
)

// Schemer wraps schema parsing methods
type Schemer interface {
	Parse(ref string) error
	IsCanonical() bool
	GetScheme() string
}

// URI implemented interface: Schemer
// Parse and determine uri scheme from the given reference
type URI struct {
	isFullPath bool
	isFullURL  bool
	hasScheme  bool
	scheme     string
}

// Parse parse the reference and determine its scheme
func (u *URI) Parse(ref string) error {
	refURL, err := url.Parse(ref)
	if err != nil {
		return err
	}

	if refURL.Scheme != "" {
		u.hasScheme = true
		u.scheme = refURL.Scheme
	}

	if refURL.Host != "" {
		u.isFullURL = true
	}

	u.isFullPath = filepath.IsAbs(refURL.Path)

	return nil
}

// IsCanonical determines whether the given address is is canonical
func (u *URI) IsCanonical() bool {
	return u.hasScheme && (u.isFullURL || u.isFullPath)
}

// GetScheme get the reference scheme name
func (u *URI) GetScheme() string {
	return u.scheme
}
