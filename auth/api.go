package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func registerBasic(c *gin.Context) {
	var basic Basic
	if err := c.Bind(&basic); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false})

		return
	}

	if err := RegisterBasic(basic.Username, basic.Password); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false})

		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true})
}

func validateBasic(c *gin.Context) {
	errMsg := "Invalid username or password"

	var basic Basic
	if err := c.Bind(&basic); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": errMsg})

		return
	}

	token, err := ValidateBasic(basic.Username, basic.Password, tokenExpiration)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "error": errMsg})

		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true, "token": token})
}

func verifyToken(c *gin.Context) {
	token := c.Param("token")
	if _, err := ValidateToken(token); err != nil {
		errMsg := "Invalid token or token has expired"
		c.JSON(http.StatusOK, gin.H{"success": false, "error": errMsg})

		return
	}

	c.JSON(http.StatusOK, gin.H{"success": true})
}

// Router auth router
func Router(router *gin.RouterGroup) {
	auth := router.Group("/auth")
	{
		token := auth.Group("/token")
		{
			token.GET("/verify/:token", verifyToken)
		}

		basic := auth.Group("/basic")
		{
			basic.POST("/register", registerBasic)
			basic.POST("/validate", validateBasic)
		}
	}
}
