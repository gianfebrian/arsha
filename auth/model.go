package auth

import (
	"errors"
	"math/rand"
	"time"

	"gitlab.com/gianfebrian/arsha/storage"
	"golang.org/x/crypto/bcrypt"
)

const (
	authTablePrefix = "auth_"
	tokenExpiration = 1 * time.Hour
)

// Basic represents basic authentication object
type Basic struct {
	ID       int    `json:"id" gorm:"PRIMARY_KEY"`
	Username string `json:"username" gorm:"INDEX"`
	Password string `json:"password"`
}

// TableName overrides tablename with telegram prefix
func (b Basic) TableName() string {
	return authTablePrefix + "basics"
}

// TokenRegistry represents token registry object for token based authentication
type TokenRegistry struct {
	Token     string    `json:"token" gorm:"PRIMARY_KEY"`
	Expiry    time.Time `json:"expiry"`
	Generated time.Time `json:"generated"`
	Basic     *Basic    `json:"basic" gorm:"foreignkey:BasicID"`
	BasicID   int
}

// TableName overrides tablename with telegram prefix
func (tr TokenRegistry) TableName() string {
	return authTablePrefix + "token_registries"
}

// RegisterBasic registers a basic object with username and password
func RegisterBasic(u, p string) error {
	b, err := bcrypt.GenerateFromPassword([]byte(p), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	basic := new(Basic)
	basic.Username = u
	basic.Password = string(b)

	storage.PG.Save(basic)

	if storage.PG.Error != nil {
		return storage.PG.Error
	}

	return nil
}

const letterBytes = "0123456789$-!+-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

// GenerateToken generates an auth token. The token will be used by Guard middleware
func GenerateToken(length int, expiry time.Duration) *TokenRegistry {
	token := new(TokenRegistry)
	token.Token = randStringBytes(25)
	token.Generated = time.Now()
	token.Expiry = token.Generated.Add(expiry)

	return token
}

// ValidateBasic validate the given auth object (username and password) using basic authentication
func ValidateBasic(u, p string, expiry time.Duration) (string, error) {
	var basic Basic
	storage.PG.Where("username = ?", u).First(&basic)
	if storage.PG.Error != nil {
		return "", storage.PG.Error
	}

	if err := bcrypt.CompareHashAndPassword([]byte(basic.Password), []byte(p)); err != nil {
		return "", err
	}

	token := GenerateToken(10, expiry)
	token.BasicID = basic.ID
	storage.PG.Save(token)
	if storage.PG.Error != nil {
		return "", storage.PG.Error
	}

	return token.Token, nil
}

// ValidateToken validates the existence and expiration time of the given token
// It will then return the token with the underlaying authentication object
func ValidateToken(t string) (*TokenRegistry, error) {
	var token TokenRegistry
	storage.PG.Where("token = ?", t).
		Preload("Basic").
		First(&token)

	if storage.PG.Error != nil {
		return nil, storage.PG.Error
	}

	if time.Now().After(token.Expiry) {
		return nil, errors.New("token has expiread")
	}

	return &token, nil
}

// RefreshToken refreshes the token with basic strategy. e.g Add some more duration from now
func RefreshToken(t *TokenRegistry, expiry time.Duration) error {
	t.Expiry = time.Now().Add(expiry)

	storage.PG.Save(t)

	return storage.PG.Error
}

// GetEntityList gets auth entity list
func GetEntityList() []interface{} {
	return []interface{}{
		new(Basic),
		new(TokenRegistry),
	}
}
