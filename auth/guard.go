package auth

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Guard guards the endpoint with authorization header token
func Guard(c *gin.Context) {
	authToken := c.GetHeader("Authorization")

	t, err := ValidateToken(authToken)
	if err != nil {
		c.AbortWithError(http.StatusForbidden, errors.New("Invalid token"))

		return
	}

	if err := RefreshToken(t, tokenExpiration); err != nil {
		c.AbortWithError(http.StatusForbidden, errors.New("Failed to refresh token"))

		return
	}

	c.Next()
}
