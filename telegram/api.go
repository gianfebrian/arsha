package telegram

import (
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/gianfebrian/arsha/auth"
	"gitlab.com/gianfebrian/arsha/storage"

	"github.com/gin-gonic/gin"
	tb "gopkg.in/tucnak/telebot.v2"
)

var ib *InlineBot

func start(c *gin.Context) {
	var payload struct {
		Token       string `json:"token"`
		ScenarioURI string `json:"scenario_uri"`
	}

	if err := c.Bind(&payload); err != nil {
		logrus.Error(err)
		c.Error(err)

		return
	}

	poller := new(tb.LongPoller)
	poller.Timeout = 10 * time.Second

	ib = new(InlineBot)
	ib.Bot, _ = tb.NewBot(tb.Settings{
		Token:  payload.Token,
		Poller: poller,
		Reporter: func(err error) {
			logrus.Error(err)
		},
	})

	err := ib.SetScenarioScript(payload.ScenarioURI)
	if err != nil {
		logrus.Error(err)
		c.Error(err)

		return
	}

	ib.Hook(nil)

	go ib.Bot.Start()

	c.JSON(http.StatusOK, gin.H{"message": "Bot is running"})
}

func stop(c *gin.Context) {
	ib.Bot.Stop()

	c.JSON(http.StatusOK, gin.H{"message": "Bot is stopping"})
}

func getMessage(c *gin.Context) {
	var messages []*Message
	storage.PG.
		Preload("From").
		Preload("Photo").
		Find(&messages)

	if storage.PG.Error != nil {
		c.Error(storage.PG.Error)
	}

	c.JSON(http.StatusOK, &messages)
}

func getCallback(c *gin.Context) {
	var callbacks []*Callback
	storage.PG.
		Preload("From").
		Preload("Message").
		Find(&callbacks)

	if storage.PG.Error != nil {
		c.Error(storage.PG.Error)
	}

	c.JSON(http.StatusOK, &callbacks)
}

// Router telegram router
func Router(router *gin.RouterGroup) {
	telegram := router.Group("/telegram")
	{
		telegram.POST("/start", auth.Guard, start)
		telegram.POST("/stop", auth.Guard, stop)

		telegram.GET("/message", auth.Guard, getMessage)
		telegram.GET("/callback", auth.Guard, getCallback)
	}
}
