package telegram

import (
	"fmt"
	"os/user"
	"path"
	"strings"
	"testing"
	"time"

	"github.com/spf13/viper"

	"github.com/spf13/afero"
	tb "gopkg.in/tucnak/telebot.v2"
)

func init() {
	viper.SetEnvPrefix("ARSHA")
	viper.AutomaticEnv()

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
}

func TestSetScenarioScript(t *testing.T) {
	u, err := user.Current()
	if err != nil {
		t.Error(err)
	}

	appFs := afero.NewOsFs()
	b, err := afero.ReadFile(appFs, "specs/scenario.json")
	if err != nil {
		t.Error(err)
	}

	afero.WriteFile(appFs, path.Join(u.HomeDir, "scenario.json"), b, 0755)

	ib := new(InlineBot)
	err = ib.SetScenarioScript("file:///" + path.Join(u.HomeDir, "scenario.json"))
	if err != nil {
		t.Error(err)
	}
}

func TestParseScenario(t *testing.T) {
	u, err := user.Current()
	if err != nil {
		t.Error(err)
	}

	appFs := afero.NewOsFs()
	b, err := afero.ReadFile(appFs, "specs/scenario.json")
	if err != nil {
		t.Error(err)
	}

	afero.WriteFile(appFs, path.Join(u.HomeDir, "scenario.json"), b, 0755)

	ib := new(InlineBot)
	err = ib.SetScenarioScript("file:///" + path.Join(u.HomeDir, "scenario.json"))
	if err != nil {
		t.Error(err)
	}

	if err := ib.ParseScenario(new(InlineScenarioParser)); err != nil {
		t.Error(err)
	}
}

func TestFindSceneByID(t *testing.T) {
	u, err := user.Current()
	if err != nil {
		t.Error(err)
	}

	appFs := afero.NewOsFs()
	b, err := afero.ReadFile(appFs, "specs/scenario.json")
	if err != nil {
		t.Error(err)
	}

	afero.WriteFile(appFs, path.Join(u.HomeDir, "scenario.json"), b, 0755)

	ib := new(InlineBot)
	err = ib.SetScenarioScript("file:///" + path.Join(u.HomeDir, "scenario.json"))
	if err != nil {
		t.Error(err)
	}

	if err := ib.ParseScenario(new(InlineScenarioParser)); err != nil {
		t.Error(err)
	}

	res := ib.findSceneByID("tuition_fee")
	if res.ID != "tuition_fee" {
		t.Errorf("Expect to get id %s, got %s", "tuition_fee", res.ID)
	}
}

func TestComposeContent(t *testing.T) {
	u, err := user.Current()
	if err != nil {
		t.Error(err)
	}

	appFs := afero.NewOsFs()
	b, err := afero.ReadFile(appFs, "specs/scenario.json")
	if err != nil {
		t.Error(err)
	}

	afero.WriteFile(appFs, path.Join(u.HomeDir, "scenario.json"), b, 0755)

	ib := new(InlineBot)
	err = ib.SetScenarioScript("file:///" + path.Join(u.HomeDir, "scenario.json"))
	if err != nil {
		t.Error(err)
	}

	if err := ib.ParseScenario(new(InlineScenarioParser)); err != nil {
		t.Error(err)
	}

	p, btn := ib.composeContent(ib.scenario[1])

	if fmt.Sprintf("%T", p) != "string" {
		t.Error("expect to get empty photo")
	}

	if len(btn) < 1 {
		t.Error("expected some inline button")
	}

	if btn[0].Unique != "main_menu" {
		t.Errorf("expected to have Unique %s, got %s", "main_menu", btn[0].Unique)
	}
}

func TestSetup(t *testing.T) {
	u, err := user.Current()
	if err != nil {
		t.Error(err)
	}

	appFs := afero.NewOsFs()
	b, err := afero.ReadFile(appFs, "specs/scenario.json")
	if err != nil {
		t.Error(err)
	}

	afero.WriteFile(appFs, path.Join(u.HomeDir, "scenario.json"), b, 0755)

	ib := new(InlineBot)
	err = ib.SetScenarioScript("file:///" + path.Join(u.HomeDir, "scenario.json"))
	if err != nil {
		t.Error(err)
	}

	if err := ib.ParseScenario(new(InlineScenarioParser)); err != nil {
		t.Error(err)
	}

	poller := new(tb.LongPoller)
	poller.Timeout = 10 * time.Second
	ib.Bot, _ = tb.NewBot(tb.Settings{
		Token:  viper.GetString("telegram.token"),
		Poller: poller,
	})

	ib.Setup()
}

func TestHook(t *testing.T) {
	u, err := user.Current()
	if err != nil {
		t.Error(err)
	}

	appFs := afero.NewOsFs()
	b, err := afero.ReadFile(appFs, "specs/scenario.json")
	if err != nil {
		t.Error(err)
	}

	afero.WriteFile(appFs, path.Join(u.HomeDir, "scenario.json"), b, 0755)

	ib := new(InlineBot)
	err = ib.SetScenarioScript("file:///" + path.Join(u.HomeDir, "scenario.json"))
	if err != nil {
		t.Error(err)
	}

	if err := ib.ParseScenario(new(InlineScenarioParser)); err != nil {
		t.Error(err)
	}

	poller := new(tb.LongPoller)
	poller.Timeout = 10 * time.Second

	ib.Bot, _ = tb.NewBot(tb.Settings{
		Token:  viper.GetString("telegram.token"),
		Poller: poller,
	})

	go func() {
		time.Sleep(500 * time.Millisecond)
		ib.Bot.Stop()
	}()

	ib.Hook(nil)
}
