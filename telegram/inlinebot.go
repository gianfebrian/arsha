package telegram

import (
	"encoding/json"
	"sort"

	"github.com/sirupsen/logrus"

	"gitlab.com/gianfebrian/arsha/bot"
	"gitlab.com/gianfebrian/arsha/reference"
	"gitlab.com/gianfebrian/arsha/storage"

	tb "gopkg.in/tucnak/telebot.v2"
)

type inlineScenario struct {
	ID          string `json:"id"`
	LayerIndex  int    `json:"layer_index"`
	Order       int    `json:"order"`
	ImageURL    string `json:"image_url"`
	Text        string `json:"text"`
	AnswerID    string `json:"answer_id"`
	BackToLayer int    `json:"back_to_layer"`
}

type InlineScenarioParser struct {
	data []byte
}

func (isp *InlineScenarioParser) SetTemplate(u string) error {
	return nil
}

func (isp *InlineScenarioParser) Read(b []byte) error {
	isp.data = b

	return nil
}

func (isp *InlineScenarioParser) Parse(container interface{}) error {
	return json.Unmarshal(isp.data, container)
}

// InlineBot implements Boter interface.
// It will act as bot with inline message interaction
type InlineBot struct {
	rawScenario []byte
	scenario    []*inlineScenario
	Bot         *tb.Bot
}

// SetScenarioScript sets scenario script to drive interaction flow
func (ib *InlineBot) SetScenarioScript(uri string) error {
	u := new(reference.URI)
	if err := u.Parse(uri); err != nil {
		return err
	}

	if u.GetScheme() == "file" {
		fl := new(reference.File)
		b, err := fl.Open(uri)
		if err != nil {
			return err
		}

		ib.rawScenario = b

		return nil
	}

	h := new(reference.HTTP)
	b, err := h.Open(uri)
	if err != nil {
		return err
	}

	ib.rawScenario = b

	return nil
}

// ParseScenario parses scenario script to an understandable format
func (ib *InlineBot) ParseScenario(p bot.Parser) error {
	if err := p.Read(ib.rawScenario); err != nil {
		return err
	}

	return p.Parse(&ib.scenario)
}

func (ib *InlineBot) findSceneByID(id string) *inlineScenario {
	for _, v := range ib.scenario {
		if v.ID == id {
			return v
		}
	}

	return nil
}

func (ib *InlineBot) composeContent(scene *inlineScenario) (interface{}, []tb.InlineButton) {
	var photo *tb.Photo
	if scene.ImageURL != "" {
		photo = new(tb.Photo)
		photo.FileURL = scene.ImageURL
	}

	var inlineButton tb.InlineButton
	var inlineButtonList []tb.InlineButton
	if scene.Text != "" {
		inlineButton = tb.InlineButton{
			Unique: scene.ID,
			Text:   scene.Text,
			Data:   scene.ID,
		}

		inlineButtonList = append(inlineButtonList, inlineButton)
	}

	if photo == nil {
		text := "Please choose one from the following menu:"
		if scene.Text != "" {
			text = scene.Text
		}

		return text, inlineButtonList
	}

	return photo, inlineButtonList
}

func (ib *InlineBot) handler(bot *tb.Bot,
	scene *inlineScenario,
	mainInlineButtonList []tb.InlineButton,
	byIndex [][]*inlineScenario) func(c *tb.Callback) {
	return func(c *tb.Callback) {

		ccb, _ := json.Marshal(c)
		logrus.WithField("cCallback", string(ccb)).Info(c.Message.Text)

		tCallback := new(Callback)
		json.Unmarshal(ccb, tCallback)
		storage.PG.Save(tCallback)

		// Remove previous content/response
		ib.Bot.Delete(c.Message)

		if scene.BackToLayer == -1 {
			if scene.AnswerID != "" {
				cbPhoto, _ := ib.composeContent(ib.findSceneByID(scene.AnswerID))

				replyMarkup := new(tb.ReplyMarkup)
				replyMarkup.InlineKeyboard = [][]tb.InlineButton{
					mainInlineButtonList,
					// cbInlineButtonList,
				}

				ib.Bot.Send(c.Sender, cbPhoto, replyMarkup)

				// Always respond
				ib.Bot.Respond(c)
			}

			return
		}

		replyMarkup := new(tb.ReplyMarkup)
		for _, layer := range byIndex[scene.BackToLayer] {
			_, layerInlineButtonList := ib.composeContent(layer)

			replyMarkup.InlineKeyboard = append(replyMarkup.InlineKeyboard, layerInlineButtonList)
		}

		ib.Bot.Send(c.Sender, scene.Text, replyMarkup)

		// Always respond
		ib.Bot.Respond(c)
	}
}

// Setup setups the bot with the given scenario script
func (ib *InlineBot) Setup() error {
	sort.Slice(ib.scenario, func(i, j int) bool {
		return ib.scenario[i].Order < ib.scenario[j].Order
	})

	byIndex := make([][]*inlineScenario, len(ib.scenario))
	for _, v := range ib.scenario {
		byIndex[v.LayerIndex] = append(byIndex[v.LayerIndex], v)
	}

	welcomePhoto, _ := ib.composeContent(byIndex[0][0])
	_, mainInlineButtonList := ib.composeContent(byIndex[0][1])
	ib.Bot.Handle(tb.OnText, func(msg *tb.Message) {
		cmsg, _ := json.Marshal(msg)
		logrus.WithField("cMessage", string(cmsg)).Info(msg.Text)

		tMessage := new(Message)
		json.Unmarshal(cmsg, tMessage)
		storage.PG.Save(tMessage)

		replyMarkup := new(tb.ReplyMarkup)
		replyMarkup.InlineKeyboard = [][]tb.InlineButton{mainInlineButtonList}

		ib.Bot.Send(msg.Sender, welcomePhoto, replyMarkup)
	})

	for i, byOrder := range byIndex {
		for j, scene := range byOrder {
			// Welcome screen will show layer 0 and order 0
			// also layer 1 and order 0
			if i == 0 && j == 0 {
				continue
			}

			_, inlineButtonList := ib.composeContent(scene)
			if len(inlineButtonList) == 0 {
				continue
			}

			ib.Bot.Handle(&inlineButtonList[0], ib.handler(ib.Bot, scene, mainInlineButtonList, byIndex))
		}
	}

	return nil
}

// Hook hooks onto messanger event e.g on message event
func (ib *InlineBot) Hook(hook bot.Hooker) error {
	if err := ib.ParseScenario(new(InlineScenarioParser)); err != nil {
		return err
	}

	return ib.Setup()
}
