package telegram

const telegramTablePrefix = "telegram_"

// User represents telegram user object e.g. recipient or sender
type User struct {
	ID        int    `json:"id" gorm:"PRIMARY_KEY"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Username  string `json:"username"`
}

// TableName overrides tablename with telegram prefix
func (u User) TableName() string {
	return telegramTablePrefix + "users"
}

// Sendable represents telegram sendable object e.g. photo
type Sendable struct {
	ID        string `json:"file_id" gorm:"PRIMARY_KEY"`
	FileLocal string `json:"file_local"`
	FilePath  string `json:"file_path"`
	FileSize  int    `json:"file_size"`
	FileURL   string `json:"file_url"`
	Height    int    `json:"height"`
	Width     int    `json:"width"`
}

// TableName overrides tablename with telegram prefix
func (u Sendable) TableName() string {
	return telegramTablePrefix + "sendables"
}

// Message represents telegram message object
type Message struct {
	ID         int   `json:"message_id" gorm:"PRIMARY_KEY"`
	Date       int   `json:"date"`
	From       *User `json:"from" gorm:"foreignkey:FromUserID"`
	FromUserID int
	Photo      *Sendable `json:"photo" gorm:"foreignkey:PhotoID"`
	PhotoID    string
	Text       string `json:"text"`
}

// TableName overrides tablename with telegram prefix
func (u Message) TableName() string {
	return telegramTablePrefix + "messages"
}

// Callback represents telegram callback object
type Callback struct {
	ID              string `json:"id" gorm:"PRIMARY_KEY"`
	Data            string `json:"data"`
	From            *User  `json:"from" gorm:"foreignkey:FromUserID"`
	FromUserID      int
	Message         *Message `json:"message" gorm:"foreignkey:MessageID"`
	MessageID       int
	InlineMessageID string `json:"inline_message_id"`
}

// TableName overrides tablename with telegram prefix
func (u Callback) TableName() string {
	return telegramTablePrefix + "callbacks"
}

// GetEntityList gets telegram entity list
func GetEntityList() []interface{} {
	return []interface{}{
		new(User),
		new(Sendable),
		new(Message),
		new(Callback),
	}
}
