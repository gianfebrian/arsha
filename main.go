package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/gianfebrian/arsha/auth"
	"gitlab.com/gianfebrian/arsha/media"
	"gitlab.com/gianfebrian/arsha/middleware"
	"gitlab.com/gianfebrian/arsha/storage"
	"gitlab.com/gianfebrian/arsha/telegram"
)

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.DebugLevel)

	if gin.Mode() == gin.ReleaseMode {
		logrus.SetLevel(logrus.InfoLevel)
	}

	viper.SetEnvPrefix("ARSHA")
	viper.AutomaticEnv()

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
}

func main() {
	if err := storage.ConnectPostgres(viper.GetString("pg.uri")); err != nil {
		panic(err)
	}

	storage.AutoMigrate(
		telegram.GetEntityList(),
		auth.GetEntityList(),
	)

	router := gin.New()
	router.Use(middleware.Logger())
	router.Use(gin.Recovery())

	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Hi, there! My name is Arsha.")
	})

	v1 := router.Group("v1")
	media.Router(v1)
	telegram.Router(v1)
	auth.Router(v1)

	host := viper.GetString("app.host")
	if host == "" {
		host = "127.0.0.1"
	}

	port := viper.GetString("app.port")
	if port == "" {
		port = "8080"
	}

	addr := fmt.Sprintf("%s:%s", host, port)
	router.Run(addr)
}
