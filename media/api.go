package media

import (
	"net/http"
	"path"

	"github.com/spf13/viper"
	"gitlab.com/gianfebrian/arsha/auth"

	"github.com/gin-gonic/gin"
)

func upload(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Failed to get the file"})
	}

	dst := path.Join(viper.GetString("media.dir"), file.Filename)
	if err := c.SaveUploadedFile(file, dst); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Failed to save the file"})
	}

	c.Status(http.StatusOK)
}

// Router media router
func Router(router *gin.RouterGroup) {
	mediaPath := viper.GetString("media.path")
	if mediaPath == "" {
		mediaPath = "/media"
	}

	media := router.Group(mediaPath)
	{
		media.Static("", viper.GetString("media.dir"))
		media.POST("/upload", auth.Guard, upload)
	}
}
