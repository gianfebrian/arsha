#!/usr/bin/env bash

CGO_ENABLED=0 go build -ldflags '-extldflags "-static"'
docker build -t arsha .
