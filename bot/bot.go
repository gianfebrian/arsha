package bot

// Hooker wraps telegram hook method e.g on message event
type Hooker interface {
	Hook() error
}

// SentinelHook implements hooker
type SentinelHook struct{}

// Hook implements sentinel hook of Hooker
func (sh *SentinelHook) Hook() error {
	return nil
}

// Parser wraps scenario configuration parser methods
type Parser interface {
	SetTemplate(uri string) error
	Read(data []byte) error
	Parse(container interface{}) error
}

// SentinelParse implements Parser
type SentinelParse struct{}

// SetTemplate implements sentinel settemplate of Parser
func (sp *SentinelParse) SetTemplate(uri string) error {
	return nil
}

// Read implements sentinel settemplate of Parser
func (sp *SentinelParse) Read(data []byte) error {
	return nil
}

// Parse implements sentinel parse of Parser
func (sp *SentinelParse) Parse(container interface{}) error {
	return nil
}

// Boter wraps bot methods
type Boter interface {
	SetScenarioScript(uri string) error
	ParseScenario(p Parser) error
	Setup() error
	Hook(hook Hooker) error
}

// SentinelBot implements Boter
type SentinelBot struct{}

// SetScenarioConfig implements sentinel SetScenarioConfig of Boter
func (sb *SentinelBot) SetScenarioScript(uri string) error {
	return nil
}

// ParseScenario implements sentinel ParseScenario of Boter
func (sb *SentinelBot) ParseScenario(p Parser) error {
	return nil
}

// Setup implements sentinel Setup of Boter
func (sb *SentinelBot) Setup() error {
	return nil
}

// Hook implements sentinel Hook of Boter
func (sb *SentinelBot) Hook(hook Hooker) error {
	return nil
}
