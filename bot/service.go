package bot

import (
	"fmt"

	"github.com/VividCortex/pm"
)

// Servicer wraps service control methods
type Servicer interface {
	SetOption(opts ServiceOption)
	Restart() error
	Reload() error
	Start() error
	Stop() error
}

// ServiceOption option for service
type ServiceOption struct {
	ID   string
	Name string
}

// SentinelService implments Servicer interface
type SentinelService struct{}

// SetOption implements sentinel SetOption of Servicer
func (sb *SentinelService) SetOption(opt ServiceOption) {}

// Restart implements sentinel Restart of Servicer
func (sb *SentinelService) Restart() error {
	return nil
}

// Reload implements sentinel Reload of Servicer
func (sb *SentinelService) Reload() error {
	return nil
}

// Start implements sentinel Start of Servicer
func (sb *SentinelService) Start() error {
	return nil
}

// Stop implements sentinel Stop of Servicer
func (sb *SentinelService) Stop() error {
	return nil
}

// Service bot service
type Service struct {
	bot Boter
	opt ServiceOption
}

// NewService creates new bot service instance
func NewService(bot Boter, opt ServiceOption) *Service {
	s := new(Service)
	s.bot = bot
	s.opt = opt

	return s
}

// SetOption sets bot service option
func (s *Service) SetOption(opt ServiceOption) {
	s.opt = opt
}

// Start starts the bot with the following cycle: read script, parse script, setup, hook
func (s *Service) Start() error {
	pm.Start(s.opt.ID, nil, nil)
	defer pm.Done(s.opt.ID)

	return s.bot.Hook(nil)
}

// Restart restarts the bot with the following cycle: read script, parse script, setup, hook
func (s *Service) Restart() error {
	if err := s.Stop(); err != nil {
		return err
	}

	return s.Start()
}

// Reload reloads the script and will attempt to inject the scenario for the next incoming interaction
func (s *Service) Reload() error {
	return nil
}

// Stop stops the bot and will attempt to shut it down gracefully if possible
func (s *Service) Stop() error {
	return pm.Kill(s.opt.ID, fmt.Sprintf("Killed %s", s.opt.Name))
}
