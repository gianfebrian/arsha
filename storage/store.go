package storage

// Saver wraps store save methods
type Saver interface {
	Save(key string, data interface{}) (int, error)
}

// Loader wraps store load methods
type Loader interface {
	Load(key string, container interface{}) error
}

// Conditioner wraps store condition methods
type Conditioner interface {
	And(field string, term interface{}) Conditioner
	Or(field string, term interface{}) Conditioner
	Condition(cond Conditioner) Conditioner
}

// Storer wraps store save, load, and condition methods
type Storer interface {
	Saver
	Loader
	Conditioner
}

// StoreSentinel sentinel implementation of storer interface
type StoreSentinel struct{}

// Save implements save method of Saver
func (ss *StoreSentinel) Save(key string, data interface{}) (int, error) {
	return 0, nil
}

// Load implements load method of Loader
func (ss *StoreSentinel) Load(key string, container interface{}) error {
	return nil
}

// And implements and method of Conditioner
func (ss *StoreSentinel) And(field string, term interface{}) Conditioner {
	return ss
}

// Or implements or method of Conditioner
func (ss *StoreSentinel) Or(field string, term interface{}) Conditioner {
	return ss
}

// Condition implements condition method of Conditioner
func (ss *StoreSentinel) Condition(cond Conditioner) Conditioner {
	return ss
}
