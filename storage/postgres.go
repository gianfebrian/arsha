package storage

import (
	"database/sql"
	"errors"

	"github.com/jinzhu/gorm"

	// Postgres driver
	_ "github.com/lib/pq"
)

// PG Globally shared connection
var PG *gorm.DB

// ConnectPostgres connect to postgres
func ConnectPostgres(args ...interface{}) error {
	if len(args) == 0 {
		return errors.New("Invalid connection argument")
	}

	var sqlDB *sql.DB

	switch args[0].(type) {
	case *sql.DB:
		sqlDB = args[0].(*sql.DB)
	case string:
		uri := args[0].(string)
		db, err := sql.Open("postgres", uri)
		if err != nil {
			return err
		}

		sqlDB = db
	default:
		return errors.New("Unknow connection argument type")
	}

	db, err := gorm.Open("postgres", sqlDB)
	if err != nil {
		return err
	}

	PG = db

	return nil
}

// AutoMigrate auto migrates entities
func AutoMigrate(entities ...[]interface{}) {
	for _, es := range entities {
		if len(es) == 0 {
			continue
		}

		for _, e := range es {
			PG.AutoMigrate(e)
		}
	}
}
