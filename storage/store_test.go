package storage

import (
	"reflect"
	"testing"
)

func TestSentinel(t *testing.T) {
	ss := new(StoreSentinel)

	savedCount, err := ss.Save("some key", "some data")
	if err != nil {
		t.Error("expected no error")
	}

	if savedCount > 0 {
		t.Error("no result should be returned")
	}

	var container string
	if err := ss.Load("some key", &container); err != nil {
		t.Error("expected no error")
	}

	and := ss.And("some field", "some term")
	if !reflect.DeepEqual(and, ss) {
		t.Error("and should return the same instance")
	}

	or := ss.Or("some field", "some term")
	if !reflect.DeepEqual(or, ss) {
		t.Error("or should return the same instance")
	}

	condition := ss.Condition(and)
	if !reflect.DeepEqual(condition, ss) {
		t.Error("condition should return the same instance")
	}
}
