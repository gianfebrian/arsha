FROM golang:1.9 as builder

ENV APP_DIR=$GOPATH/src/gitlab.com/gianfebrian/arsha

RUN go get -u github.com/kardianos/govendor

WORKDIR $APP_DIR

ADD vendor vendor

RUN govendor sync -v

COPY . .

RUN CGO_ENABLED=0 go install -ldflags '-extldflags "-static"'

FROM golang:1.9-alpine3.7

COPY --from=builder /go/bin/arsha /go/bin/arsha

ENTRYPOINT [ "arsha" ]
